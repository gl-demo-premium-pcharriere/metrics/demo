# demo

## Setup

### Create 2 scoped labels

![01](pictures/01-labels.png)

### Create a VSA "from scratch"

![02](pictures/02-vsa.png)

### Create a VSA with the default template

![03](pictures/03-vsa.png)

### Create a PAT with API rights access

![04](pictures/04-pat.png)

## What is the CI-CD pipeline is doing?

- At every commit on MR, the `mr_checking_with_glab` check the labels of the current MR
  - If it not exists, the job will add the ~feature::start label
  - The review application is deployed
- When the MR is merged, 
  - The job will remove the ~feature::start label
  - The job will add the ~feature::end label

## Results
> 👋 you need to do MR and deployments several times per day durin at least one week

### Visual Stream Analytics Graphs

![05](pictures/05-metrics.png)

### Dora Metrics
> The deployment frequency will change

![06](pictures/06-dora.png)

#### If you use the Jira Issues instead of the GitLab Issues

- Measure DORA metrics with Jira
  - Deployment frequency and Lead time for changes are calculated based on GitLab CI/CD and Merge Requests (MRs), **and do not require Jira data**.
  - Time to restore service and Change failure rate require GitLab incidents for the calculation. For more information, see [Measure DORA Time to restore service and Change failure rate with **external incidents**](https://docs.gitlab.com/ee/user/analytics/dora_metrics.html#measure-dora-time-to-restore-service-and-change-failure-rate-with-external-incidents).
  - https://docs.gitlab.com/ee/user/analytics/dora_metrics.html#measure-dora-metrics-with-jira

- We are planning an implementation to calculate the other Dora metrics from the Jira Issues: 
  - https://gitlab.com/gitlab-org/gitlab/-/issues/348626
  - https://gitlab.com/gitlab-org/gitlab/-/issues/342780


## Improvements

### TODO

- create a VSA stage to follow the deployment of the review applications
